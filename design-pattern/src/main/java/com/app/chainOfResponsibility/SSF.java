package com.app.chainOfResponsibility;

public class SSF extends Handler {

    @Override
    public double applyTax(Invoice invoice) {
        invoice.setTax(invoice.getTax() + invoice.getAmount() * 0.04);
        System.out.println("SSF");
        return invoice.getTax();
    }
}
