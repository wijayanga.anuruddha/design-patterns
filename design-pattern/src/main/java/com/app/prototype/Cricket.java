package com.app.prototype;

public class Cricket extends Sport {

    private int overs;

    public int getOvers() {
        return overs;
    }

    public void setOvers(int overs) {
        this.overs = overs;
    }

    @Override
    public String toString() {
        return "Cricket{" +
                "overs=" + overs +
                '}';
    }
}
