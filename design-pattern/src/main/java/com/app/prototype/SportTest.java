package com.app.prototype;

public class SportTest {

    public static void main(String[] args) {

        SportRegistry sportRegistry = new SportRegistry();

        Cricket cricket = (Cricket) sportRegistry.getSport(SportName.Cricket);
        System.out.println(cricket);

        cricket.setOvers(20);
        System.out.println(cricket);

        Cricket cricket1 = (Cricket) sportRegistry.getSport(SportName.Cricket);
        System.out.println(cricket1);
    }
}
