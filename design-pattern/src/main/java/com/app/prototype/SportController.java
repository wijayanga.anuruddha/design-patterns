package com.app.prototype;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SportController {

    @GetMapping("/sports")
    public Sport getSport (@RequestParam(name = "sport") String sport){
        return new SportRegistry().getSportUsingAPI(sport);
    }
}
