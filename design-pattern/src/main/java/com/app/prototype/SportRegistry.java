package com.app.prototype;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class SportRegistry {

    private Map<SportName, Sport> sports = new HashMap<>();

    public SportRegistry() {
        this.initialize();
    }

    public Sport getSport (SportName sportName){
        Sport sport = null;
        try {
            sport = (Sport) sports.get(sportName).clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return sport;
    }

    public Sport getSportUsingAPI (String sportName){
        Sport sport = null;
        try {
            sport = (Sport) sports.get(SportName.valueOf(sportName)).clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return sport;
    }

    private void initialize() {
        Cricket cricket = new Cricket();
        cricket.setType("Outdoor");
        cricket.setPlayers(11);
        cricket.setOvers(50);

        Score score = new Score();
        score.setType("Outdoor");
        score.setPlayers(11);
        score.setTimePeriod(90);

        sports.put(SportName.Cricket, cricket);
        sports.put(SportName.Score, score);
    }
}
