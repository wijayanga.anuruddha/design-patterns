package com.app.prototype;

public class Score extends Sport {

    private double timePeriod;

    public double getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(double timePeriod) {
        this.timePeriod = timePeriod;
    }

    @Override
    public String toString() {
        return "Score{" +
                "timePeriod=" + timePeriod +
                '}';
    }
}
