package com.app.factory;

public class SliverPackage extends Package {
    @Override
    protected void createPackage() {
        tours.add(new Flight());
        tours.add(new Accommodation());
    }
}
