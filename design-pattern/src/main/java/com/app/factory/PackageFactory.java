package com.app.factory;

public class PackageFactory {

    public static Package createPackage(PackageCode packageCode){
        switch (packageCode){
            case BASIC:
                return new BasicPackage();
            case SILVER:
                return new SliverPackage();
            case PLATINUM:
                return new PlatinumPackage();
            default:
                return null;
        }
    }
}
