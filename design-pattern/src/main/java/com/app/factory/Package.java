package com.app.factory;

import java.util.ArrayList;
import java.util.List;

public abstract class Package {

    protected List<Tour> tours = new ArrayList<>();

    public Package(){
        createPackage();
    }

    protected abstract void createPackage();

    @Override
    public String toString() {
        return "Package{" +
                "tours=" + tours +
                '}';
    }
}
