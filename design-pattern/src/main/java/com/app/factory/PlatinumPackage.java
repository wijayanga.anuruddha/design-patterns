package com.app.factory;

public class PlatinumPackage extends Package {
    @Override
    protected void createPackage() {
        tours.add(new Flight());
        tours.add(new Accommodation());
        tours.add(new Transport());
    }
}
