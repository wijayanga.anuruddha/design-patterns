package com.app.factory;

public class BasicPackage extends Package {

    @Override
    protected void createPackage() {
        tours.add(new Flight());
    }
}
