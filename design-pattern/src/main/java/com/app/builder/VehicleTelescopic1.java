package com.app.builder;

public class VehicleTelescopic1 {

    private String insurance;
    private Boolean etc;
    private String type;
    private String model;

    public VehicleTelescopic1(String insurance) {
        this.insurance = insurance;
    }

    public VehicleTelescopic1(String insurance, Boolean etc) {
        this(insurance);
        this.etc = etc;
    }

    public VehicleTelescopic1(String insurance, Boolean etc, String type) {
        this(insurance, etc);
        this.type = type;
    }

    public VehicleTelescopic1(String insurance, Boolean etc, String type, String model) {
        this(insurance, etc, type);
        this.model = model;
    }

    @Override
    public String toString() {
        return "VehicleTelescopic1{" +
                "insurance='" + insurance + '\'' +
                ", etc=" + etc +
                ", type='" + type + '\'' +
                ", model='" + model + '\'' +
                '}';
    }
}
