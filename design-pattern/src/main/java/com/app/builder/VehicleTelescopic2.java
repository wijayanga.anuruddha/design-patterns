package com.app.builder;

public class VehicleTelescopic2 {

    private String insurance;
    private Boolean etc;
    private String type;
    private String model;

    public VehicleTelescopic2(String insurance, Boolean etc, String type, String model) {
        this.insurance = insurance;
        this.etc = etc;
        this.type = type;
        this.model = model;
    }

    public VehicleTelescopic2(String insurance, Boolean etc, String type) {
        this(insurance, etc, type, null);
    }

    public VehicleTelescopic2(String insurance, Boolean etc) {
        this(insurance, etc, null, null);
    }

    public VehicleTelescopic2(String insurance) {
        this(insurance, null, null, null);
    }

    @Override
    public String toString() {
        return "VehicleTelescopic2{" +
                "insurance='" + insurance + '\'' +
                ", etc=" + etc +
                ", type='" + type + '\'' +
                ", model='" + model + '\'' +
                '}';
    }
}
