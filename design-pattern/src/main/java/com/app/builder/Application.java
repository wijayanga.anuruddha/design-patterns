package com.app.builder;

public class Application {

    public static void main(String[] args) {

        VehicleTelescopic1 vehicleTelescopic1 = new VehicleTelescopic1("Fully");
        System.out.println(vehicleTelescopic1);

        VehicleTelescopic2 vehicleTelescopic2 = new VehicleTelescopic2("Fully");
        System.out.println(vehicleTelescopic2);

        Vehicle.Builder builder = new Vehicle.Builder("Fully");
        Vehicle vehicle = builder.type("Honda").model("Civic").build();
        System.out.println(vehicle);
    }
}
