package com.app.builder;

public class Vehicle {

    private String insurance;
    private Boolean etc;
    private String type;
    private String model;

    public Vehicle(Builder builder) {
        this.insurance = builder.insurance;
        this.etc = builder.etc;
        this.type = builder.type;
        this.model = builder.model;
    }

    static class Builder {

        private String insurance;
        private Boolean etc;
        private String type;
        private String model;

        public Vehicle build(){
            return new Vehicle(this);
        }

        public Builder (String insurance){
            this.insurance= insurance;
        }

        public Builder etc (Boolean etc){
            this.etc = etc;
            return this;
        }

        public Builder type(String type){
            this.type = type;
            return this;
        }

        public Builder model (String model){
            this.model = model;
            return this;
        }
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "insurance='" + insurance + '\'' +
                ", etc=" + etc +
                ", type='" + type + '\'' +
                ", model='" + model + '\'' +
                '}';
    }
}
