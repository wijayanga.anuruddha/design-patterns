package com.app.memento;

public class Application {

    public static void main(String[] args) {

        CareTaker careTaker = new CareTaker();

        Cart cart = new Cart();
        cart.addItem(new Item("Cake"));
        cart.addItem(new Item("Bread"));

        careTaker.save(cart);
        System.out.println(cart);

        cart.addItem(new Item("Butter"));
        careTaker.save(cart);
        System.out.println(cart);

        cart.addItem(new Item("Yogurt"));
        careTaker.save(cart);
        System.out.println(cart);

        careTaker.revert(cart);
        System.out.println(cart);

        careTaker.revert(cart);
        System.out.println(cart);

    }
}
