package com.app.singleton;

public class DBManager {

    private static volatile DBManager dbManager;

    private DBManager() {
        if (dbManager != null) {
            throw new RuntimeException("Please use getDBManager method");
        }
    }

    public static DBManager getDbManager() {
        if (dbManager == null) {
            synchronized (DBManager.class) {
                if (dbManager == null) {
                    dbManager = new DBManager();
                }
            }
        }
        return dbManager;
    }
}
