package com.app;

import com.app.prototype.Cricket;
import com.app.prototype.SportName;
import com.app.prototype.SportRegistry;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class PrototypeTest {

    @InjectMocks
    private SportRegistry sportRegistry;

    @Test
    public void getSport_test() {
        Cricket cricket = (Cricket) sportRegistry.getSport(SportName.Cricket);
        assertEquals(cricket.getOvers(), 50);
    }

    @Test
    public void getSportWithChangeOvers_test() {
        Cricket cricket = (Cricket) sportRegistry.getSport(SportName.Cricket);
        cricket.setOvers(20);
        assertEquals(cricket.getOvers(), 20);
    }

    @Test
    public void getSportCheckOver_test() {
        Cricket cricket = (Cricket) sportRegistry.getSport(SportName.Cricket);
        assertEquals(cricket.getOvers(), 50);
    }
}
